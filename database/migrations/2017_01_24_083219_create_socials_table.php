<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sosmed')->unique();
            $table->string('url');
            $table->timestamps();
        });

        DB::table('socials')->insert([
        'sosmed' => 'Facebook',
        'url' => 'http://facebook.com/url',
		'created_at' => date('Y-m-d')
        ]);

        DB::table('socials')->insert([
        'sosmed' => 'Twitter',
        'url' => 'http://twitter.com/url',
		'created_at' => date('Y-m-d')
        ]);

        DB::table('socials')->insert([
        'sosmed' => 'Youtube',
        'url' => 'http://youtube.com/url',
		'created_at' => date('Y-m-d')
        ]);
        
        DB::table('socials')->insert([
        'sosmed' => 'Pinterest',
        'url' => 'http://pinterest.com/url',
		'created_at' => date('Y-m-d')
        ]);
        DB::table('socials')->insert([
        'sosmed' => 'Instagram',
        'url' => 'http://instragram.com/url',
		'created_at' => date('Y-m-d')
        ]);
        DB::table('socials')->insert([
        'sosmed' => 'Mail',
        'url' => 'mail@site.com',
		'created_at' => date('Y-m-d')
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('socials');
    }
}
