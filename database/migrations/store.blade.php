@extends('admin.layouts.app')

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"><a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Manage Store Location</a></div>
    <h1>Manage Store Location</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a class="btn-success btn-new-data" href="{{URL::to('dashboard/store/create')}}">Create New Data</a>        
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Store Location List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Store Name</th>
                  <th>Alamat</th>
                  <th>Kota</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($stores as $data)
                  <tr>
                  {!! Form::open([
                      'method' => 'delete',
                      'route' => ['store.destroy', $data->id]
                  ])!!}
                    <td>{{$data->name}}</td> 
                    <td>{{$data->address}}</td> 
                    <td>{{$data->city}}</td> 
                    <td>
                      {!! Html::linkRoute('store-edit', 'Edit', array($data->id),  ['class' => 'btn btn-warning']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    </td> 
                  {!! Form::close() !!}
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection