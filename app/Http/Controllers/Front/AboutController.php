<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AboutController extends Controller
{
    public function index(){
        $content = DB::table('text_contents')
                ->where('page', 'About')
                ->get();
        $sidebar = DB::table('image_contents')
                ->where('page', 'About - Sidebar')
                ->get();
        
        $page = "about";
        return view('frontend.section.about')
        	->with('content', $content)
        	->with('sidebar', $sidebar)
            ->with('page', $page);
    }
}
