<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class GalleryController extends Controller
{
    public function index(){
    	$image = DB::table('image_contents')
                ->where('page', 'Gallery')
                ->get();

        $page = "gallery";
        return view('frontend.section.gallery')
        	->with('image', $image)
            ->with('page',$page);
    }
}
