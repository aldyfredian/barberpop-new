<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ContactController extends Controller
{
    public function index(){
    	$sidebar = DB::table('image_contents')
                ->where('page', 'Contact - Sidebar')
                ->get();
        $page = "contact";
        return view('frontend.section.contact')
        	->with('sidebar', $sidebar)
            ->with('page',$page);
    }

    public function store(Request $request){
    	$this->validate($request, [
            'subject' => 'required',
            'email' => 'required|email'
        ]);

        $this->msg->subject = $request->subject;
        $this->msg->email = $request->email;
        $this->msg->message = $request->message;
        $this->msg->save();

        if($request->hasFile('url')){
            $file = $request->file('url');
            $path = 'uploads/message/';

            $thumbnail = Image::make($request->file('url'));
            $thumbnail->save($path.$this->msg->id.".".$file->getClientOriginalExtension());
            $thumbnail->resize(80,80)->save($path."thumbs-".$this->msg->id.".".$file->getClientOriginalExtension());
            $this->msg->url = $this->msg->id.".".$file->getClientOriginalExtension();
        }
        $this->msg->save();
        return route('contact.index');
    }
}
