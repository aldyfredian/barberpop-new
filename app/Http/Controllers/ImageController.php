<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Image as Img;
use File;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $image = Image::all();
        $code = 3;
        return view('admin.section.image-content.list')
            ->with('image', $image)
            ->with('code', $code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $image = New Image;
        $action = 'image.store';
        $code = 3;
        return view('admin.section.image-content.form')
            ->with('image', $image)
            ->with('action', $action)
            ->with('code', $code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'page' => 'required'
        ]);

        $image = New Image;

        $arrays = array(
                'Home - Sidebar' => true,
                'About - Sidebar' => true,
                'Prices & Services - Sidebar' => true,
                'Contact - Sidebar' => true,
                'Franchise - Sidebar' => true
            );

        $count = 0;

        if(isset($arrays[$request->page]) && $arrays[$request->page]) {
            $count = Image::where('page', $request->page)->count();
        }
        
        if($count == 0){
            $image->page = $request->page;
            $image->caption = $request->caption;
            $image->save();

            if($request->hasFile('image')){
                $file = $request->file('image');
                $path = 'uploads/image/';

                $thumbnail = Img::make($request->file('image'));

                //Full Size Image
                $thumbnail->save($path.$image->id.".".$file->getClientOriginalExtension());

                // For Gallery Only
                if($image->page == 'Gallery')
                {
                    $thumbnail->fit(160, 160)->save($path."gallery/".$image->id.".".$file->getClientOriginalExtension());
                }

                $image->url = $image->id.".".$file->getClientOriginalExtension();
            }
            $image->save();
            return redirect()->route('image.index');
        } else {
            return redirect()->route('image.create')
                ->with('error2', 'Data Sudah Ada!!!');
        }            
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::find($id);
        $action = 'image.update';
        $code = 3;
        return view('admin.section.image-content.form')
            ->with('image', $image)
            ->with('action', $action)
            ->with('code', $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Image::find($id);

        $image->caption = $request->caption;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $path = 'uploads/image/';

            // Delete Old Files
            File::delete($path.$image->url);
            // Delete Gallery Old File
            if($image->page == 'Gallery'){
                File::delete($path.'/gallery'.$image->url);
            }

            $thumbnail = Img::make($request->file('image'));

            //Full Size Image
            $thumbnail->save($path.$image->id.".".$file->getClientOriginalExtension());

            // For Gallery Only
            if($image->page == 'Gallery')
            {
                $thumbnail->fit(160, 160)->save($path."gallery/".$image->id.".".$file->getClientOriginalExtension());
            }
            $image->url = $image->id.".".$file->getClientOriginalExtension();
        }
        $image->save();
        return redirect()->route('image.index');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
