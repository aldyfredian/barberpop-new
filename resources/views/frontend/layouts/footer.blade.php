
<div id="footer" class="container-fluid"> 
    <div class="container">
        <div class="row footer-content">
            <div class="col-sm-4">
                <h4 class="footer-title">INSTAGRAM FEED</h4>           
        <iframe src="http://lightwidget.com/widgets/8304e98aeb265ed29254b21bea41f54e.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
      </div>
            <div class="col-sm-4">
                <h4 class="footer-title">TWITTER FEED</h4>
        <a class="twitter-timeline" data-width="400" data-height="500" href="https://twitter.com/BARBER_POP">Tweets by Barberpop</a>
            </div>
            <div class="col-sm-4">
                <h4 class="footer-title">FIND BARBERPOP ON</h4>
                <ul class="social">
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="copyright col-sm-12">
                <span class="logo-footer"><img src="assets/front/img/logo-white.png"/></span>
                <span>Barberpop Inc. 2018 &copy; <a href="http://pentacode.id">Pentacode Digital</a>.</span>
            </div>
        </div>
    </div>
</div>