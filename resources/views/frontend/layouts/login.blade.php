<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap -->
    <link href="{{URL::to('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- style -->
    <link href="{{URL::to('assets/css/style.css')}}" rel="stylesheet">
    <!-- Colorbox -->
    <link href="{{URL::to('assets/css/colorbox.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::to('assets/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{URL::to('js/app.js')}}"></script>
    <script src="{{URL::to('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--Twitter Widget-->
    <script async src="{{URL::to('https://platform.twitter.com/widgets.js')}}"></script>
    <!--Instagram Widget-->
    <script src="http://lightwidget.com/widgets/lightwidget.js"></script>
    <title>Login</title>
    <link rel="icon" href="{{URL::to('assets/favicon.ico')}}" type="image/x-icon">
</head>
<body>

    @yield('content')
    <script src="{{URL::to('assets/admin/js/jquery.dataTables.min.js')}}"></script> 
    <script src="{{URL::to('assets/admin/js/matrix.tables.js')}}"></script>
</body>
</html>
