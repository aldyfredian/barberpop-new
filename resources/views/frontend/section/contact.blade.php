@extends('frontend.layouts.app')

@section('content')
  <div id="content" class="container" style="height:100%">
    <div class="row">
      <div class="col-sm-8">
        <div class="col-sm-12 general-title">
            <h4>CONTACT US</h4>
        </div>
         {!! Form::open(['action' => 'Front\ContactController@store', 'enctype' =>'multipart/form-data']) !!}
        <div class="control-group">
          <div class="row margin-15">
            <div class="col-sm-12">  
              @if (session('status'))
                <div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                  <strong>Terima kasih atas pesan anda,</strong> Kami akan merespon pesan anda secepat mungkin. 
                </div>
              @endif
            </div>
          </div>
          <div class="row margin-15">            
            <div class="col-sm-3">SUBJECT</div>
            {!! Form::text('subject', null, ['class' => 'col-sm-9']) !!}
            @if ($errors->has('subject'))
                <span class="help-block col-sm-offset-3" style="color: #f4645f">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
          </div>
          <div class="row margin-15">            
            <div class="col-sm-3">EMAIL ADDRESS</div>
            {!! Form::text('email', null, ['class' => 'col-sm-9']) !!}
            @if ($errors->has('email'))
                <span class="help-block col-sm-offset-3" style="color: #f4645f">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
          <div class="row margin-15">            
            <div class="col-sm-3">ATTACH FILE</div>
            {!! Form::file('url', null, ['class' => 'col-sm-9']) !!}
          </div>
          <div class="row margin-15">            
            <div class="col-sm-3">MESSAGE</div>
            {!! Form::textarea('message', null, ['class' => 'col-sm-9']) !!}
          </div>
          <div class="row margin-15">
            {!! Form::submit('Send', ['class' => 'col-sm-offset-3 col-sm-1 submit-btn']) !!}
          </div>
      </div>
      </div>
      {!! Form::close() !!}
      <div class="col-sm-4 right-img">
       @foreach($sidebar as $data)
            <div class="sidebar" style="background-image: url('{{URL::to('uploads/image/'.$data->url)}}'); background-size: cover; background-position: center;">
            </div>
        @endforeach
    </div>
  </div>
</div>
@endsection
