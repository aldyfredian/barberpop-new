@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container-fluid">
    <div class="row">
        <div class="col-sm-8 slider-full">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                @php $isFirst = true; $i=0; @endphp
                <ol class="carousel-indicators">
                    @foreach($slider as $data)
                    <li data-target="#carousel-example-generic" data-slide-to="{{ $i++ }}" class="@if($isFirst) @php echo 'active'; $isFirst = false; @endphp @endif"></li>
                    @endforeach
                </ol>
                <!-- Wrapper for slides -->
                @php $isFirst = true; @endphp
                <div class="carousel-inner" role="listbox">
                    @foreach($slider as $data)
                    <div class="item @if($isFirst) @php echo 'active'; $isFirst = false; @endphp @endif">
                        <img src="{{URL::to('uploads/image/'.$data->url)}}" alt="Gambar">
                    </div>
                    @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-sm-4 right-img">
            @foreach($sidebar as $data)
            <div class="sidebar" style="background-image: url('{{URL::to('uploads/image/'.$data->url)}}'); background-size: cover; background-position: center;">
            </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 home-message">
            <marquee>
                Barberpop Cimanuk Bandung: 022-7279230 | 
                Barberpop Saka Bandung: 022-82602495 | 
                Barberpop Dago Bandung: 022-87804876 | 
                Barberpop Hairmechanic: @barberpop_hairmechanic | 
                Barberpop Sukabumi: 026-6219388 | 
                Barberpop Makassar: 041-1832156 | 
                Barberpop Medan: 062-4562980 | 
                Barberpop Batam: 077-87493694 | 
                Barberpop Manado: 043-18821663 | 
                Barberpop Palembang: 071-137918 | 
                Barberpp Denpasar Bali: 036-1221172 | 
                Barberpop Balikpapan: @barberpop_balikpapan | 
                Barberpop_kendari: @barberpop_kendari | 
            </marquee>
        </div>
    </div>
    <div class="container">
        <!-- Lookbook -->        
        <div class="row">
            <div class="col-sm-12 lookbook-title">
                <h3>&bull; LOOKBOOK &bull;</h3>
            </div>
            <div class="col-sm-3 lookbook-block">
                @foreach($lbleft as $data)
                <p>{{$data->content}}</p>
                @endforeach
            </div>
            <div class="col-sm-6 lookbook-block">
                @foreach($lookbook as $data)
                <div class="col-sm-3 col-xs-6 mmb-15">
                    <div class="lookbook-picture">
                        <img src="{{URL::to('uploads/image/'.$data->url)}}" alt="lookbook"/>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-sm-3 lookbook-block">
                @foreach($lbright as $data)
                <p>{{$data->content}}</p>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection