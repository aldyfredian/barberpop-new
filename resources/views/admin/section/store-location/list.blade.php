@extends('admin.layouts.app')

@section('addCSS')
    <style>
        body{
            overflow-y: hidden;
        }
    </style>
@endsection

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Table Styles Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Store Location</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="{{ URL::to('adm') }}">Home</a></li>
                        <li>Store Location</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Table Styles Header -->
    <!-- Datatables Block -->
    <!-- Datatables is initialized in js/pages/uiTables.js -->
    <div class="block full">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-add-new">
                    <a href="{{ URL::to('adm/store/create') }}" class="btn btn-danger">Add New</a> 
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Store Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($store as $data)
                        {!! Form::open([ 'method' => 'delete', 'route' => ['store.destroy', $data->id]]) !!}
                        <tr>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->address }}</td>
                            <td>{{ $data->city }}</td>
                            <td style="text-align: center;">
                                <a class="btn btn-warning btn-xs btn-icon" href="{{ URL::to('adm/store/'.$data->id.'/edit') }}"><i class="fa fa-pencil"></i></a>
                                <button type="submit" class="btn btn-danger btn-xs btn-icon"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        {!! Form::close() !!}
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
</div>
<!-- END Page Content -->
@endsection

@section('addJS')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{ URL::asset('assets/js/pages/uiTables.js') }}"></script>
    <script>$(function(){ UiTables.init(); });</script>
@endsection