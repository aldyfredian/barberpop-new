@extends('admin.layouts.app')

@section('addCSS')
<style>
    body{
        overflow-y: hidden;
    }
</style>

@endsection

@section('content')
<!-- Page content -->
    <div id="page-content">
        <!-- Forms Components Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        @if($action == 'image.store')
                            <h1>Add New Image</h1>
                        @else
                            <h1>{{ $image->page }}</h1>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li><a href="{{ URL::to('adm') }}">Home</a></li>
                            <li><a href="{{ URL::to('adm/image') }}">Image Contents</a></li>                            
                            @if($action == 'image.store')
                                <li>Add Content</li>
                            @else
                                <li>Edit Content</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Forms Components Header -->

        <!-- Form Components Row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form Block -->
                <div class="block">
                    <!-- Horizontal Form Content -->
                    @if($action == 'image.store')
                        {!! Form::open(['route' => $action, 'class' => 'form-horizontal form-bordered', 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::model($image, ['route' => [$action, $image->id], 'class'=> 'form-horizontal form-bordered', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                        <div class="form-group">
                            <label class="col-md-2 control-label">Page / Position</label>
                            <div class="col-md-10">
                                @if($action == 'image.store')
                                    {!! Form::select('page', [
                                        'Home - Slideshow' => 'Home - Slideshow',
                                        'Home - Sidebar' => 'Home - Sidebar', 
                                        'Home - Lookbook' => 'Home - Lookbook',
                                        'About - Sidebar' => 'About - Sidebar',
                                        'Prices & Services - Pricing' => 'Prices & Services - Pricing', 
                                        'Prices & Services - Sidebar' => 'Prices & Services - Sidebar', 
                                        'Shop - Products' => 'Shop - Products',
                                        'Gallery' => 'Gallery',
                                        'Contact - Sidebar' => 'Contact - Sidebar',
                                        'Franchise - Sidebar' => 'Franchise - Sidebar'],
                                        $image->page, ['class' => 'form-control'])
                                    !!}
                                @else
                                    {!! Form::select('page', [
                                        'Home - Slideshow' => 'Home - Slideshow',
                                        'Home - Sidebar' => 'Home - Sidebar', 
                                        'Home - Lookbook' => 'Home - Lookbook',
                                        'About - Sidebar' => 'About - Sidebar',
                                        'Prices & Services - Pricing' => 'Prices & Services - Pricing', 
                                        'Prices & Services - Sidebar' => 'Prices & Services - Sidebar', 
                                        'Shop - Products' => 'Shop - Products',
                                        'Gallery' => 'Gallery',
                                        'Contact - Sidebar' => 'Contact - Sidebar',
                                        'Franchise - Sidebar' => 'Franchise - Sidebar'],
                                        $image->page, ['class' => 'form-control', 'disabled'])
                                    !!}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Image File</label>
                            <div class="col-md-10">
                                {!! Form::file('image', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Caption</label>
                            <div class="col-md-10">
                                {!! Form::text('caption', $image->caption , ['class' => 'form-control']) !!}
                            </div>
                        </div>                                
                        <div class="form-group form-actions">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- END Horizontal Form Content -->
                </div>
                <!-- END Horizontal Form Block -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection