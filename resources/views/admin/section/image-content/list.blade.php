@extends('admin.layouts.app')

@section('addCSS')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/lightbox.css') }}">
@endsection

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Table Styles Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Image Contents</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="{{ URL::to('adm') }}">Home</a></li>
                        <li>Image Contents</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Table Styles Header -->
    <!-- Datatables Block -->
    <!-- Datatables is initialized in js/pages/uiTables.js -->
    <div class="block full">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-add-new">
                    <a href="{{ URL::to('adm/image/create') }}" class="btn btn-danger">Add New</a> 
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Page / Position</th>
                        <th>Caption</th>
                        <th width="80px">Image</th>
                        <th width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($image as $data)
                    <tr>
                        <td>{{ $data->page }}</td>
                        <td>{{ $data->caption }}</td>
                        @if($data->url)
                            <td style="text-align: center;"><a href="{{ URL::asset('uploads/image').'/'.$data->url }}" class="btn btn-primary btn-xs btn-icon" data-lightbox="gallery"><i class="fa fa-eye"></i></a></td>
                        @else
                            <td style="text-align: center;">No Image</td>
                        @endif
                        <td style="text-align: center;"><a class="btn btn-warning btn-xs btn-icon" href="{{ URL::to('adm/image/'.$data->id.'/edit') }}"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
</div>
<!-- END Page Content -->
@endsection

@section('addJS')
    <script src="{{ URL::asset('assets/js/lightbox.js') }}"></script>
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{ URL::asset('assets/js/pages/uiTables.js') }}"></script>
    <script>$(function(){ UiTables.init(); });</script>
@endsection