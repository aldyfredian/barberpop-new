@extends('admin.layouts.app')

@section('addCSS')
<style>
    body{
        overflow-y: hidden;
    }
</style>

@endsection

@section('content')
<!-- Page content -->
    <div id="page-content">
        <!-- Forms Components Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>{{ $social->sosmed }}</h1>
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li><a href="{{ URL::to('adm') }}">Home</a></li>
                            <li><a href="{{ URL::to('adm/social') }}">Text Contents</a></li>
                            <li>Edit Content</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Forms Components Header -->

        <!-- Form Components Row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form Block -->
                <div class="block">
                    <!-- Horizontal Form Content -->
                    {!! Form::model($social, ['route' => ['social.update', $social->id], 'class'=> 'form-horizontal form-bordered', 'method' => 'PUT']) !!}
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Social Media</label>
                            <div class="col-md-10">
                                {!! Form::text('sosmed', $social->sosmed, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Account URL</label>
                            <div class="col-md-10">
                                {!! Form::text('url', $social->url, ['class' => 'form-control']) !!}
                            </div>
                        </div>		                                
                        <div class="form-group form-actions">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- END Horizontal Form Content -->
                </div>
                <!-- END Horizontal Form Block -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection