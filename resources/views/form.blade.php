@extends('admin.layouts.app')

@section('content')
<!-- Page content -->
    <div id="page-content">
        <!-- Forms Components Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Form Default</h1>
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Home</li>
                            <li>Form Default</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Forms Components Header -->

        <!-- Form Components Row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form Block -->
                <div class="block">
                    <!-- Horizontal Form Content -->
                    <form action="page_forms_components.html" method="post" class="form-horizontal form-bordered" onsubmit="return false;">
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Email</label>
                            <div class="col-md-10">
                                <input type="email" id="example-hf-email" name="example-hf-email" class="form-control">
                                <span class="help-block">Please enter your email</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-password">Password</label>
                            <div class="col-md-10">
                                <input type="password" id="example-hf-password" name="example-hf-password" class="form-control">
                                <span class="help-block">Please enter your password</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-password">Password</label>
                            <div class="col-md-10">
                                <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"></textarea>
                                <span class="help-block">Please enter your password</span>
                            </div>
                        </div>		                                
                        <div class="form-group form-actions">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Horizontal Form Content -->
                </div>
                <!-- END Horizontal Form Block -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection