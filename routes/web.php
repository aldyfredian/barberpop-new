<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'Front\HomeController');
Route::resource('/about', 'Front\AboutController');
Route::resource('/prices-and-services', 'Front\PnSController');
Route::resource('/gallery', 'Front\GalleryController');
Route::resource('/location', 'Front\LocationController');;
Route::resource('/contact', 'Front\ContactController');
Route::resource('/franchise', 'Front\FranchiseController');

Route::prefix('adm')->group(function () {

    // Dashboard
    Route::resource('/', 'AdminController');
    //

    // Text Content
    Route::resource('text', 'TextController');

    // Image Content
    Route::resource('image', 'ImageController');

    // Social Media
    Route::resource('social', 'SocialController');

    // Store Location
    Route::resource('store', 'StoreController');

    // Visitor Message
    Route::resource('message', 'MessageController');
});